##Welcome

This source code repository contains data anaysis code and data 
files developed for CressExpress 2.0, an on-line tool for 
co-expression analysis. The on-line tool is implemented (mostly)
in Java. 

This repository contains R code we used to prototype
analyses, check integrity of data, and document co-expression analysis
focused on specific genes or pathways.

The code here mostly depends on a database running in Amazon RDS.
To use the database, contact Ann Loraine for read-only credentials.

## About folders in this repository

Folders contained in this repository represent data analysis "modules" that are mostly independent
unites but sometimes use files and results from other modules.

### SomeAgainstAll

This module shows how to compare a set of query genes (probesets) to all other genes (probe sets) on an array. Arrays are from the ATH1 platform and data are from the Gene Expression Omnibus.

### ExternalDataSets

Contains data files downloaded from other Web sites or generated in upstream bioinformatics data processing steps.



