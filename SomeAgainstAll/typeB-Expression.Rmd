Global expression profiles of type B response regulators
=====================

Introduction
------------

This Markdown profiles expression of type B response regulator genes over many thousands of experiments. 

Questions to answer include:

* Which type B RR's are widely expressed in many sample types?
* Which type B RR's are less widely expressed?

Analysis
--------

### Read cytokinin related genes

```{r}
fname='../ExternalDataSets/CytokininPathwayGenes.xlsx'
suppressPackageStartupMessages(library('xlsx'))
cyto=read.xlsx(fname,sheetIndex=1)
cyto$Symbol=as.character(cyto$Symbol)
cyto$AGI=as.character(cyto$AGI)
cyto$Gene.type=as.character(cyto$Gene.type)
```

### Read and merge annotations and probe set to gene mappings:

```{r}
fname='../ExternalDataSets/ps_info.txt.gz'
annots=read.delim(fname,as.is=T,header=T,sep='\t')
```

### Get probe sets, agi codes, symbols for type B RRs

```{r}
typeB=cyto[grep('Type B ARR',cyto$Gene.type),]
pss_indexes=which(annots$AGI%in%typeB$AGI)
pss=annots$ps[pss_indexes] 
agis=annots$AGI[pss_indexes]
row.names(typeB)=typeB$AGI
symbols=typeB[agis,]$Symbol # ARR1 has two probesets
```

### Get names and order of arrays in microarray table, dumped from release table

```{r}
# file from database dump
fname='../ExternalDataSets/r4.0/cressExpressFlatFileDump/microarrayOrder.txt'
microarrayOrder=read.delim(fname,header=F,as.is=T)[,1]
```

There were `r length(microarrayOrder)` array names.

### Expression data for every probe set for every array

```{r}
# a release Ann made summer 2013
fname='../ExternalDataSets/r4.0/cressExpressFlatFileDump/exprRowData.csv.gz'
# 22,801 rows, around 8,900 columns
# takes about a minute to read it, on Apple 2014 laptop 2.6 GHz Intel Core i7, 16 GB RAM 
d=read.delim(fname,header=F,as.is=T,sep=',')
row.names(d)=d[,1]
d=d[,-1]
names(d)=microarrayOrder
```

There were `r dim(d)[1]` rows and `r dim(d)[2]` columns of microarray expression values in `r fname`.

### Plot type B RR expression patterns

```{r fig.width=10,fig.height=8}
par(mfrow=c(3,4))
for (i in 1:length(pss)) {
  sym=symbols[i]
  ps=pss[i]
  agi=agis[i]
  vals=as.numeric(d[ps,])
  main=paste(sym,paste0('(',ps,')'))
  hist(vals,main=main,xlab="Expression",ylab="Arrays")
}
quartz(file='results/typeBHistograms.png',dpi=600,type='png',
       width=8,height=6)
par(mfrow=c(3,4))
for (i in 1:length(pss)) {
  sym=symbols[i]
  ps=pss[i]
  agi=agis[i]
  vals=as.numeric(d[ps,])
  main=paste(sym,paste0('(',ps,')'))
  hist(vals,main=main,xlab="Expression",ylab="Arrays")
}
dev.off()
par(mfrow=c(1,1))
```

Conclusion
----------

Only ARR10, ARR1, ARR14, and ARR2 have symmetrical expression profiles, suggesting they are the most widely expressed. 

ARR13 and ARR21 are barely expressed. The one probe set that measures them (250612_s_at) has little or no signal in the majority of arrays. It would be interesting to investigate the arrays where they are detectable.

ARR19 is also weakly expressed, with distribution similar in shape to ARR13/ARR21. 

Two probe sets (256790_at and 257649_at) measure ARR1. The overall shape of the distribution is similar but by no means identical. It would be interesting to investigate why there are two probe sets for this gene.  

