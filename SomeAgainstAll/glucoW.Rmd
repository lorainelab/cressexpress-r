Example co-expression
=====================

Introduction
------------

This Markdown file demonstrates CressExpress co-expression analysis using genes from glucosinolates biosynthesis pathway from tryptophan (W).

Questions to answer include:

* How many genes are co-expressed with one or more genes in the pathway?

Analysis
--------

Probe sets:

```{r echo=FALSE}
# glucosinolate biosynthesis from tryptophan
baitProbesets = c('252827_at', '264052_at', '253534_at', '263714_at', '264873_at', '260387_at')
symbols=c('CYP79B2', 'CYP79B3', 'CYP83B1', 'SUR1', 'UGT74B1', 'AT1G74100')
```

Names and order of arrays in microarray table, dumped from release table:

```{r}
# file from database dump
fname='../ExternalDataSets/r4.0/cressExpressFlatFileDump/microarrayOrder.txt'
microarrayOrder=read.delim(fname,header=F,as.is=T)[,1]
```

There were `r length(microarrayOrder)` array names.

Read table with expression values for every probe set for every array in microarrayOrder:

```{r}
# a release Ann made summer 2013
fname='../ExternalDataSets/r4.0/cressExpressFlatFileDump/exprRowData.csv.gz'
# 22,801 rows, around 8,900 columns
# takes about a minute to read it, on Apple 2014 laptop 2.6 GHz Intel Core i7, 16 GB RAM 
d=read.delim(fname,header=F,as.is=T,sep=',')
row.names(d)=d[,1]
d=d[,-1]
names(d)=microarrayOrder
```

There were `r dim(d)[1]` rows and `r dim(d)[2]` columns of microarray expression values in `r fname`.

Pick arrays:

```{r}
set.seed(23) # always pick the same ones
numArrays=500
microarraySelections=sample(microarrayOrder,size=numArrays)
```

Compare bait probeset expression values to expression values for all the other probesets for `r numArrays` arrays:


```{r}
cors=numeric(nrow(d)*length(baitProbesets)) 
resultsIndex=1 # because R
for (otherProbeset in row.names(d)) {
  x=as.numeric(d[otherProbeset,microarraySelections])
  for (baitProbeset in baitProbesets) {
    y = as.numeric(d[baitProbeset,microarraySelections])
    cors[resultsIndex]=cor(x,y)
    resultsIndex=resultsIndex+1
  }
} 
cors.save=cors # need this later
```

Make output files
-----------------

Read annotations:

```{r}
fname='../ExternalDataSets/ps_info.txt.gz'
annots=read.delim(fname,as.is=T,header=T,sep='\t')
# some map to more than one gene
annots=annots[!duplicated(annots$ps),c('ps','unique','redundant','AGI','symbol','descr')]
# some probe sets have no target gene
annots=merge(data.frame(ps=row.names(d)),annots,by.x='ps',by.y='ps',all.x=T)
```

Assemble results in a data frame we can write to disk:

This was helpful:

* http://www.r-tutor.com/r-introduction/matrix/matrix-construction

```{r}
cors=matrix(cors,nrow=nrow(d),ncol=length(baitProbesets),byrow=T)
cors=data.frame(cors)
names(cors)=paste0('cor.',baitProbesets) 
# R won't allow column names that start w/ number
# R users need to read data file, so prepend "cor"
cors$ps=row.names(d)
results=merge(annots,cors,by.x='ps',by.y='ps')
index=grep('cor',names(results))[1]
o=order(results[,index],decreasing=T)
results=results[o,] # sort by correlation of first probe set
```

Write results to disk:

```{r}
fname='results/glucoW.tsv' # glucosinolates from tryptophan (W)
write.table(results,fname,row.names=F,sep='\t',quote=F)
```

How many genes are co-expressed with one or more bait probesets? (Don't need to report this in CressExpress.)

```{r}
r=0.6
v=cors.save>=r
cors.mat=matrix(v,nrow=nrow(d),ncol=length(baitProbesets),byrow=T)
num.coexpressed=apply(cors.mat,2,sum)
names(num.coexpressed)=symbols
num.coexpressed
```

How many co-epxressed genes are co-expressed with all, almost all, some, and so on?

```{r}
nc=apply(cors.mat,1,sum) # number of bait genes co-expressed with
table(nc)
```

Find genes that are co-expressed with 2 or more bait probesets and sort by the number of bait probe sets they are co-expressed with - this is pathway level co-expression:

```{r}
colnames(cors.mat)=baitProbesets
rownames(cors.mat)=row.names(d)
names(nc)=row.names(d)
nc.any=nc[nc>=1]
nc=nc[nc>=2]
o=order(nc,decreasing=T)
nc=nc[o]
cors.mat=cors.mat[names(nc),]
row.names(results)=results$ps
plc=results[names(nc),]
plc=data.frame(nc=nc,plc)
plc[,c('nc','AGI','symbol','descr')]
```

Write pathway level co-expression:

```{r}
fname='results/glucoW-plc.tsv'
write.table(plc,fname,row.names=F,sep='\t',quote=F)
```

Conclusion
----------

Many genes are co-expressed with multiple bait genes. These genes probably have something to do with glucosinolate biosynthesis from tryptophan. 

* There were `r length(nc.any)` genes that were co-expressed with one or more bait probesets.
* There were `r length(nc)` genes that were co-expressed with two or more bait probesets.

